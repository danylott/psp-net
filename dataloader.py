import os
import torch
from torch.utils import data
from PIL import Image
import numpy as np
from torchvision.transforms import ToTensor
import torchvision.transforms.functional as ff


class MyTupleDataset(data.Dataset):
    def __init__(self, image_folder, label_folder, crop_size=(152, 200)):
        self.image_folder = image_folder
        self.label_folder = label_folder
        self.y_cls = np.ones(9)
        self.crop_size = crop_size

        self.images = [image for image in os.listdir(self.image_folder) if image.endswith('.jpg')
                       or image.endswith('.JPG') or image.endswith('.png') or image.endswith('.PNG')
                       or image.endswith('.jpeg') or image.endswith('.JPEG')]
        self.labels = [label for label in os.listdir(self.label_folder) if label.endswith('.jpg')
                       or label.endswith('.JPG') or label.endswith('.png') or label.endswith('.PNG')
                       or label.endswith('.jpeg') or label.endswith('.JPEG')]

        super(MyTupleDataset, self).__init__()

    def __getitem__(self, index):
        # img_file = self.images[index]
        # label_file = self.labels[index]

        # image = Image.open(self.image_folder + img_file)
        # label = Image.open(self.label_folder + label_file).convert('LA')

        # image = ToTensor()(image).unsqueeze(0)
        # # im_arr = np.array(image)
        # # image = torch.from_numpy(im_arr)
        # # image = im_tensor.permute(2, 0, 1)

        # lbl_arr = np.array(label)
        # label = torch.from_numpy(lbl_arr)
        # # label = lbl_tensor.permute(2, 0, 1).long()

        # self.y_cls = np.append(self.y_cls, [1, 1])

        # return image, label, torch.from_numpy(self.y_cls)
        img_file = self.images[index]
        # print(img_file)
        label_file = self.labels[index]

        image = Image.open(self.image_folder + img_file)
        label = Image.open(self.label_folder + label_file)

        image, label = self.center_crop(image, label, self.crop_size)
        # label = np.array(label)
        # label = Image.fromarray(label.astype('uint8'))

        # _image = np.array(image)
        # image = torch.from_numpy(_image).to(torch.float)
        # image = image[np.newaxis, :]
        # image = image.permute(0, 3, 1, 2)
        image = ToTensor()(image).unsqueeze(0).to(torch.float)
        # print(image.size())
        label = ToTensor()(label).to(torch.long)[0]
        label = label.unsqueeze(0)
        # print(label)
        # image = torch.Tensor(image)
        # label = torch.Tensor(label).to(torch.long)

        # print('Info: ',image.size(), label.size())

        # for idx, cls in self.y_cls:
        #     self.y_cls[idx] = 1


        return image, label, torch.from_numpy(self.y_cls).unsqueeze(0)

    def center_crop(self, img, label, crop_size):
        img = ff.center_crop(img, crop_size)
        label = ff.center_crop(label, crop_size)

        return img, label


if __name__ == "__main__":
    dataset = MyTupleDataset('images/images/', 'images/labels/')
    img, lbl, lbl_cls = dataset[0]
    print(type(img), type(lbl), type(lbl_cls))
